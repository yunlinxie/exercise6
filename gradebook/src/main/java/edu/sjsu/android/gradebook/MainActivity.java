package edu.sjsu.android.gradebook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import edu.sjsu.android.dataprovider.StudentsProvider;

public class MainActivity extends AppCompatActivity {
    private final Uri uri = StudentsProvider.CONTENT_URI;
    MainActivity binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void addStudent(View view) {
        ContentValues values = new ContentValues();
        values.put("name",((EditText) findViewById(R.id.studentName)).getText().toString());
        values.put("grade",((EditText) findViewById(R.id.studentGrade)).getText().toString());
        // Toast message if successfully inserted
        if (getContentResolver().insert(uri, values) != null)
            Toast.makeText(this, "Student Added", Toast.LENGTH_SHORT).show();
    }
    public void getAllStudents(View view) {
// Sort by student name
        try (Cursor c = getContentResolver().
                query(uri, null, null, null, "name")) {
            if (c.moveToFirst()) {
                String result = "Yunlin Xie's Gradebook: \n";
                do {
                    for (int i = 0; i < c.getColumnCount(); i++) {
                        result = result.concat
                                (c.getString(i) + "\t");
                    }
                    result = result.concat("\n");
                } while (c.moveToNext());
                TextView textview = findViewById(R.id.result);
                textview.setText(result);
            }
        }
    }
}